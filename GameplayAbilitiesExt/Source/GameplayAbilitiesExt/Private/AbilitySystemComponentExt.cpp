// Copyright 2019 RATE Games


#include "AbilitySystemComponentExt.h"

#include "AbilitySystemGlobals.h"
#include "GameplayCueManager.h"

void UAbilitySystemComponentExt::BeginPlay()
{
	Super::BeginPlay();

	// Register for Gameplay Tag events
	RegisterGenericGameplayTagEvent().AddUObject(this, &UAbilitySystemComponentExt::OnTagChanged);

	AbilityCommittedCallbacks.AddUObject(this, &UAbilitySystemComponentExt::OnAbilityCommitted);
	//OnAbilityEnded.AddUObject(this, &UAbilitySystemComponentExt::OnAbilityFinished);
}

FGameplayAbilitySpecHandle UAbilitySystemComponentExt::RegisterAbility(TSubclassOf<UGameplayAbility> AbilityClass, int32 Level)
{
	if (AbilityClass)
	{
		return GiveAbility(FGameplayAbilitySpec(AbilityClass, Level));
	}

	return FGameplayAbilitySpecHandle();
}

void UAbilitySystemComponentExt::UpdateAttributeValue(const FGameplayAttribute Attribute, float Value)
{
	ApplyModToAttribute(Attribute, EGameplayModOp::Override, Value);
}

void UAbilitySystemComponentExt::UpdateBaseDamageAttributeType(const FGameplayTag AttributeTag)
{
	GameplayTagCountContainer.SetTagCount(AttributeTag, 1);
}

UAbilitySystemComponentExt* UAbilitySystemComponentExt::GetAbilitySystemComponentFromActor(const AActor* Actor, bool LookForComponent)
{
	return Cast<UAbilitySystemComponentExt>(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor, LookForComponent));
}

void UAbilitySystemComponentExt::OnTagChanged(const FGameplayTag Tag, int32 NewCount)
{
	OnGameplayTagUpdated.Broadcast(Tag, NewCount);
}

void UAbilitySystemComponentExt::OnAbilityCommitted(UGameplayAbility* Ability)
{
	OnGameplayAbilityCommitted.Broadcast(Ability);
}

/*
void UAbilitySystemComponentExt::OnAbilityFinished(const FAbilityEndedData& AbilityData)
{
	OnGameplayAbilityEnded.Broadcast(AbilityData.AbilityThatEnded, AbilityData.bWasCancelled);
}
*/

FGameplayEffectCollectionSpec UAbilitySystemComponentExt::AddTargetsToEffectContainerSpec(const FGameplayEffectCollectionSpec & ContainerSpec, const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors)
{
	FGameplayEffectCollectionSpec NewSpec = ContainerSpec;
	NewSpec.AddTargets(HitResults, TargetActors);
	return NewSpec;
}

TArray<FActiveGameplayEffectHandle> UAbilitySystemComponentExt::ApplyExternalEffectContainerSpec(const FGameplayEffectCollectionSpec & ContainerSpec)
{
	TArray<FActiveGameplayEffectHandle> AllEffects;

	// Iterate list of gameplay effects
	for (const FGameplayEffectSpecHandle& SpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
	{
		if (SpecHandle.IsValid())
		{
			// If effect is valid, iterate list of targets and apply to all
			for (TSharedPtr<FGameplayAbilityTargetData> Data : ContainerSpec.TargetData.Data)
			{
				AllEffects.Append(Data->ApplyGameplayEffectSpec(*SpecHandle.Data.Get()));
			}
		}
	}
	return AllEffects;
}

void UAbilitySystemComponentExt::CanActivateAbilities(const FGameplayTagContainer& AbilityTags, TArray<bool>& AbilityStatus)
{
	TArray<bool> Statuses;

	TArray<FGameplayAbilitySpec*> Specs;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(AbilityTags, Specs, true);

	for (FGameplayAbilitySpec* Spec : Specs)
	{
		UGameplayAbility* Instance = Spec->GetPrimaryInstance();
		if (Instance != nullptr)
		{
			FGameplayAbilityActorInfo ActorInfo = Instance->GetActorInfo();
			Statuses.Add(Instance->CanActivateAbility(Spec->Handle, &ActorInfo));
		}
		else
		{
			Statuses.Add(false);
		}
	}

	AbilityStatus = Statuses;
}


void UAbilitySystemComponentExt::ExecuteGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters & GameplayCueParameters)
{
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::Executed, GameplayCueParameters);
}

void UAbilitySystemComponentExt::AddGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters & GameplayCueParameters)
{
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::OnActive, GameplayCueParameters);
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::WhileActive, GameplayCueParameters);
}

void UAbilitySystemComponentExt::RemoveGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters & GameplayCueParameters)
{
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::Removed, GameplayCueParameters);
}

void UAbilitySystemComponentExt::ExecuteGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UAbilitySystemComponent* const AbilitySystemComponent = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(TargetActor);
	AbilitySystemComponent->ExecuteGameplayCue(GameplayCueTag, GameplayCueParameters);
}

void UAbilitySystemComponentExt::AddGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UAbilitySystemComponent* const AbilitySystemComponent = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(TargetActor);
	AbilitySystemComponent->AddGameplayCue(GameplayCueTag, GameplayCueParameters);
}

void UAbilitySystemComponentExt::RemoveGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag)
{
	UAbilitySystemComponent* const AbilitySystemComponent = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(TargetActor);
	AbilitySystemComponent->RemoveGameplayCue(GameplayCueTag);
}

FActiveGameplayEffectHandle UAbilitySystemComponentExt::ApplyGameplayEffectInstanceToTarget(UGameplayEffect *GameplayEffect, UAbilitySystemComponent *Target, float Level, FGameplayEffectContextHandle Context)
{
	return ApplyGameplayEffectToTarget(GameplayEffect, Target, Level, MakeEffectContext());
}

FActiveGameplayEffectHandle UAbilitySystemComponentExt::ApplyGameplayEffectInstanceToSelf(UGameplayEffect *GameplayEffect, float Level, FGameplayEffectContextHandle Context)
{
	return ApplyGameplayEffectToSelf(GameplayEffect, Level, MakeEffectContext());
}

void UAbilitySystemComponentExt::CancelAbilities(const FGameplayTagContainer& WithTags, const FGameplayTagContainer& WithoutTags)
{
	Super::CancelAbilities(&WithTags, &WithoutTags);
}

void UAbilitySystemComponentExt::CancelAllAbilities()
{
	Super::CancelAllAbilities();
}
