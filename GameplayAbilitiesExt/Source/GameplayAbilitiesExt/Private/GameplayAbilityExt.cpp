// Copyright 2019 RATE Games


#include "GameplayAbilityExt.h"

#include "GameFramework/Character.h"
#include "AbilitySystemGlobals.h"
#include "AbilitySystemComponent.h"

#include "TargetType.h"



UGameplayAbilityExt::UGameplayAbilityExt() {}

FGameplayEffectCollectionSpec UGameplayAbilityExt::MakeEffectContainerSpecFromContainer(const FGameplayEffectCollection& Container, const FGameplayEventData& EventData, int32 OverrideGameplayLevel)
{
	// First figure out our actor info
	FGameplayEffectCollectionSpec ReturnSpec;
	AActor* OwningActor = GetOwningActorFromActorInfo();
	ACharacter* OwningCharacter = Cast<ACharacter>(OwningActor);
	UAbilitySystemComponent* OwningASC = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(OwningActor);

	if (OwningASC)
	{
		// If we have a target type, run the targeting logic. This is optional, targets can be added later
		if (Container.TargetType.Get())
		{
			TArray<FHitResult> HitResults;
			TArray<AActor*> TargetActors;
			const UTargetTypeExt* TargetTypeCDO = Container.TargetType.GetDefaultObject();
			AActor* AvatarActor = GetAvatarActorFromActorInfo();
			TargetTypeCDO->GetTargets(OwningCharacter, AvatarActor, EventData, HitResults, TargetActors);
			ReturnSpec.AddTargets(HitResults, TargetActors);
		}

		// If we don't have an override level, use the default on the ability itself
		if (OverrideGameplayLevel == INDEX_NONE)
		{
			OverrideGameplayLevel = OverrideGameplayLevel = this->GetAbilityLevel(); //OwningASC->GetDefaultAbilityLevel();
		}

		// Build GameplayEffectSpecs for each applied effect
		for (const TSubclassOf<UGameplayEffect>& EffectClass : Container.TargetGameplayEffectClasses)
		{
			ReturnSpec.TargetGameplayEffectSpecs.Add(MakeOutgoingGameplayEffectSpec(EffectClass, OverrideGameplayLevel));
		}
	}
	return ReturnSpec;
}

FGameplayEffectCollectionSpec UGameplayAbilityExt::MakeEffectContainerSpec(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel)
{
	FGameplayEffectCollection* FoundContainer = EffectContainerMap.Find(ContainerTag);

	if (FoundContainer)
	{
		return MakeEffectContainerSpecFromContainer(*FoundContainer, EventData, OverrideGameplayLevel);
	}
	return FGameplayEffectCollectionSpec();
}

TArray<FActiveGameplayEffectHandle> UGameplayAbilityExt::ApplyEffectContainerSpec(const FGameplayEffectCollectionSpec& ContainerSpec)
{
	TArray<FActiveGameplayEffectHandle> AllEffects;

	// Iterate list of effect specs and apply them to their target data
	for (const FGameplayEffectSpecHandle& SpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
	{
		AllEffects.Append(K2_ApplyGameplayEffectSpecToTarget(SpecHandle, ContainerSpec.TargetData));
	}
	return AllEffects;
}

TArray<FActiveGameplayEffectHandle> UGameplayAbilityExt::ApplyEffectContainer(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel)
{
	FGameplayEffectCollectionSpec Spec = MakeEffectContainerSpec(ContainerTag, EventData, OverrideGameplayLevel);
	return ApplyEffectContainerSpec(Spec);
}

void UGameplayAbilityExt::ExecuteGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UAbilitySystemComponent* const AbilitySystemComponent = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(TargetActor);
	AbilitySystemComponent->ExecuteGameplayCue(GameplayCueTag, GameplayCueParameters);
}

void UGameplayAbilityExt::AddGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UAbilitySystemComponent* const AbilitySystemComponent = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(TargetActor);
	AbilitySystemComponent->AddGameplayCue(GameplayCueTag, GameplayCueParameters);
}

void UGameplayAbilityExt::RemoveGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag)
{
	UAbilitySystemComponent* const AbilitySystemComponent = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(TargetActor);
	AbilitySystemComponent->RemoveGameplayCue(GameplayCueTag);
}

/**
 * Removed because use GetAbilitySystemComponentFromActorInfo means ability cannot be Non-Instanced
 * Better to implement this using macros in blueprint
 * 
void UGameplayAbilityExt::CheckInterceptAbility(TEnumAsByte<EInterceptPins> & Outcome)
{
	// By default, set the outcome to failure, and only change if everything else executed successfully
	Outcome = EInterceptPins::Continue;

	if (IntercepterTag.IsValid() &&
		GetAbilitySystemComponentFromActorInfo()->HasMatchingGameplayTag(IntercepterTag))
	{
		Outcome = EInterceptPins::Intercept;
	}
}
*/