// Copyright 2019 RATE Games


#include "Tasks/AbilityTask_WaitGameplayEventOrTimeout.h"

#include "TimerManager.h"
#include "AbilitySystemGlobals.h"
#include "AbilitySystemComponent.h"

// ----------------------------------------------------------------
UAbilityTask_WaitGameplayEventOrTimeout::UAbilityTask_WaitGameplayEventOrTimeout(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

UAbilityTask_WaitGameplayEventOrTimeout* UAbilityTask_WaitGameplayEventOrTimeout::WaitGameplayEventOrTimeout(UGameplayAbility* OwningAbility, float Timeout, FGameplayTag Tag, AActor* OptionalExternalTarget, bool OnlyTriggerOnce, bool OnlyMatchExact)
{
	UAbilityTask_WaitGameplayEventOrTimeout* MyObj = NewAbilityTask<UAbilityTask_WaitGameplayEventOrTimeout>(OwningAbility);
	MyObj->Tag = Tag;
	MyObj->SetExternalTarget(OptionalExternalTarget);
	MyObj->Timeout = Timeout;
	MyObj->OnlyTriggerOnce = OnlyTriggerOnce;
	MyObj->OnlyMatchExact = OnlyMatchExact;

	return MyObj;
}

void UAbilityTask_WaitGameplayEventOrTimeout::Activate()
{
	UAbilitySystemComponent* ASC = GetTargetASC();
	if (ASC)
	{
		if (OnlyMatchExact)
		{
			MyHandle = ASC->GenericGameplayEventCallbacks.FindOrAdd(Tag).AddUObject(this, &UAbilityTask_WaitGameplayEventOrTimeout::GameplayEventCallback);
		}
		else
		{
			MyHandle = ASC->AddGameplayEventTagContainerDelegate(FGameplayTagContainer(Tag), FGameplayEventTagMulticastDelegate::FDelegate::CreateUObject(this, &UAbilityTask_WaitGameplayEventOrTimeout::GameplayEventContainerCallback));
		}

		UWorld* World = GetWorld();
		TimeStarted = World->GetTimeSeconds();

		// Use a dummy timer handle as we don't need to store it for later but we don't need to look for something to clear
		World->GetTimerManager().SetTimer(TimerHandle, this, &UAbilityTask_WaitGameplayEventOrTimeout::OnTimeFinish, Timeout, false);
	}

	Super::Activate();
}

void UAbilityTask_WaitGameplayEventOrTimeout::GameplayEventCallback(const FGameplayEventData* Payload)
{
	GameplayEventContainerCallback(Tag, Payload);
}

void UAbilityTask_WaitGameplayEventOrTimeout::GameplayEventContainerCallback(FGameplayTag MatchingTag, const FGameplayEventData* Payload)
{
	if (ShouldBroadcastAbilityTaskDelegates())
	{
		FGameplayEventData TempPayload = *Payload;
		TempPayload.EventTag = MatchingTag;
		EventReceived.Broadcast(MoveTemp(TempPayload));
	}
	if (OnlyTriggerOnce)
	{
		EndTask();
	}
}

void UAbilityTask_WaitGameplayEventOrTimeout::OnTimeFinish()
{
	if (ShouldBroadcastAbilityTaskDelegates())
	{
		OnTimeout.Broadcast(FGameplayEventData());
	}
	EndTask();
}

void UAbilityTask_WaitGameplayEventOrTimeout::SetExternalTarget(AActor* Actor)
{
	if (Actor)
	{
		UseExternalTarget = true;
		OptionalExternalTarget = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor);
	}
}

UAbilitySystemComponent* UAbilityTask_WaitGameplayEventOrTimeout::GetTargetASC()
{
	if (UseExternalTarget)
	{
		return OptionalExternalTarget;
	}

	return AbilitySystemComponent;
}

void UAbilityTask_WaitGameplayEventOrTimeout::OnDestroy(bool AbilityEnding)
{
	// Clear Timer
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);

	// De-register event callbacks
	UAbilitySystemComponent* ASC = GetTargetASC();
	if (ASC && MyHandle.IsValid())
	{
		if (OnlyMatchExact)
		{
			ASC->GenericGameplayEventCallbacks.FindOrAdd(Tag).Remove(MyHandle);
		}
		else
		{
			ASC->RemoveGameplayEventTagContainerDelegate(FGameplayTagContainer(Tag), MyHandle);
		}
	}

	Super::OnDestroy(AbilityEnding);
}