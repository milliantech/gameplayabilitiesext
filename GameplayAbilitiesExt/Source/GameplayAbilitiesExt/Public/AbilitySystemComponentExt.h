// Copyright 2019 RATE Games

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"

#include "AbilityTypes.h"
#include "GameplayEffect.h"

#include "AbilitySystemComponentExt.generated.h"

// Delegate to inform GameplayTag added, updated or removed
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnGameplayTagUpdateDelegate, const FGameplayTag, Tag, int32, NewCount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameplayAbilityCommiteDelegate, UGameplayAbility*, Ability);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnGameplayAbilityEndeDelegate, UGameplayAbility*, Ability, bool, bWasCancelled);

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameplayAbilityActivateDelegate, const FGameplayTag, AbilityTag);


/**
 * 
 */
UCLASS()
class GAMEPLAYABILITIESEXT_API UAbilitySystemComponentExt : public UAbilitySystemComponent
{
	GENERATED_BODY()
	
public:

	virtual void BeginPlay() override;

	/** 
	 * Register ability from blueprint. 
	 * Will not store registered abilities in this class, so do it in blueprints.
	 */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	FGameplayAbilitySpecHandle RegisterAbility(TSubclassOf<UGameplayAbility> AbilityClass, int32 Level = 1);

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void UpdateAttributeValue(const FGameplayAttribute Attribute, float Value);

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void UpdateBaseDamageAttributeType(const FGameplayTag AttributeTag);

	/** Version of function in AbilitySystemGlobals that returns correct type */
	static UAbilitySystemComponentExt* GetAbilitySystemComponentFromActor(const AActor* Actor, bool LookForComponent = false);

	/** Add targets to effect container for later execution */
	UFUNCTION(BlueprintCallable, Category = "Abilities", meta = (AutoCreateRefTerm = "HitResults,TargetActors"))
	static FGameplayEffectCollectionSpec AddTargetsToEffectContainerSpec(const FGameplayEffectCollectionSpec& ContainerSpec, const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors);

	/** Apply effect container to targets */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	static TArray<FActiveGameplayEffectHandle> ApplyExternalEffectContainerSpec(const FGameplayEffectCollectionSpec& ContainerSpec);

	//////////// Gameplay Cue methods ////////////

	/** Execute Gameplay Cue locally only */
	UFUNCTION(BlueprintCallable, Category = "GameplayCue", Meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void ExecuteGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	/** Add Gameplay Cue execution locally only */
	UFUNCTION(BlueprintCallable, Category = "GameplayCue", Meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void AddGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	/** Remove Gameplay Cue execution locally only */
	UFUNCTION(BlueprintCallable, Category = "GameplayCue", Meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void RemoveGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	/** Execute Gameplay Cue execution on a target */
	UFUNCTION(BlueprintCallable, Category = "GameplayCue", Meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void ExecuteGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	/** Add Gameplay Cue execution to a target */
	UFUNCTION(BlueprintCallable, Category = "GameplayCue", Meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void AddGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	/** Remove Gameplay Cue execution from a target */
	UFUNCTION(BlueprintCallable, Category = "GameplayCue", Meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void RemoveGameplayCueToTarget(const AActor* TargetActor, const FGameplayTag GameplayCueTag);

	//////////// Attributes methods ////////////

	UFUNCTION(BlueprintCallable, Category = "Abilities|Attributes")
	FActiveGameplayEffectHandle ApplyGameplayEffectInstanceToTarget(UGameplayEffect *GameplayEffect, UAbilitySystemComponent *Target, float Level, FGameplayEffectContextHandle Context);

	UFUNCTION(BlueprintCallable, Category = "Abilities|Attributes")
	FActiveGameplayEffectHandle ApplyGameplayEffectInstanceToSelf(UGameplayEffect *GameplayEffect, float Level, FGameplayEffectContextHandle Context);

	/** Cancel all abilities with the specified tags. Will not cancel the Ignore instance */
	UFUNCTION(BlueprintCallable, Category = "Abilities|Attributes", Meta = (AutoCreateRefTerm = "WithTags, WithoutTags"))
	void CancelAbilities(const FGameplayTagContainer& WithTags, const FGameplayTagContainer& WithoutTags);

	/** Cancels all abilities regardless of tags. Will not cancel the ignore instance */
	UFUNCTION(BlueprintCallable, Category = "Abilities|Attributes")
	void CancelAllAbilities();

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Abilities")
	void CanActivateAbilities(const FGameplayTagContainer& AbilityTags, TArray<bool>& AbilityStatus);


private:

	// Delegates from ability system
	UPROPERTY(BlueprintAssignable)
	FOnGameplayTagUpdateDelegate OnGameplayTagUpdated;

	UPROPERTY(BlueprintAssignable)
	FOnGameplayAbilityCommiteDelegate OnGameplayAbilityCommitted;

	UPROPERTY(BlueprintAssignable)
	FOnGameplayAbilityEndeDelegate OnGameplayAbilityEnded;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnGameplayAbilityActivateDelegate OnGameplayAbilityActivated;

	UFUNCTION()
	void OnTagChanged(const FGameplayTag Tag, int32 NewCount);

	UFUNCTION()
	void OnAbilityCommitted(UGameplayAbility* Ability);

	//UFUNCTION()
	//void OnAbilityFinished(const FAbilityEndedData& AbilityData);
};
