// Copyright 2019 RATE Games


#include "Async/AttributeChangedAsyncTask.h"
#include "AttributeSet.h"

UAttributeChangedAsyncTask* UAttributeChangedAsyncTask::ListenForAttributeChange(UAbilitySystemComponent* AbilitySystemComponent, FGameplayAttribute Attribute)
{
	UAttributeChangedAsyncTask* WaitForAttributeChangedTask = NewObject<UAttributeChangedAsyncTask>();
	WaitForAttributeChangedTask->ASC = AbilitySystemComponent;
	WaitForAttributeChangedTask->AttributeToListenFor = Attribute;

	if (!IsValid(AbilitySystemComponent) || !Attribute.IsValid())
	{
		WaitForAttributeChangedTask->RemoveFromRoot();
		return nullptr;
	}

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(Attribute)
		.AddUObject(WaitForAttributeChangedTask, &UAttributeChangedAsyncTask::AttributeChanged);

	return WaitForAttributeChangedTask;
}

UAttributeChangedAsyncTask* UAttributeChangedAsyncTask::ListenForAttributeChangeWithMax(UAbilitySystemComponent* AbilitySystemComponent, FGameplayAttribute Attribute, FGameplayAttribute MaxAttribute)
{
	UAttributeChangedAsyncTask* WaitForAttributeChangedTask = NewObject<UAttributeChangedAsyncTask>();
	WaitForAttributeChangedTask->ASC = AbilitySystemComponent;
	WaitForAttributeChangedTask->AttributeToListenFor = Attribute;
	WaitForAttributeChangedTask->MaxAttributeToCompare = MaxAttribute;

	if (!IsValid(AbilitySystemComponent) || !Attribute.IsValid() || !MaxAttribute.IsValid())
	{
		WaitForAttributeChangedTask->RemoveFromRoot();
		return nullptr;
	}

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(Attribute)
		.AddUObject(WaitForAttributeChangedTask, &UAttributeChangedAsyncTask::AttributeChanged);

	return WaitForAttributeChangedTask;
}

UAttributeChangedAsyncTask * UAttributeChangedAsyncTask::ListenForAttributesChange(UAbilitySystemComponent * AbilitySystemComponent, TArray<FGameplayAttribute> Attributes)
{
	UAttributeChangedAsyncTask* WaitForAttributeChangedTask = NewObject<UAttributeChangedAsyncTask>();
	WaitForAttributeChangedTask->ASC = AbilitySystemComponent;
	WaitForAttributeChangedTask->AttributesToListenFor = Attributes;

	if (!IsValid(AbilitySystemComponent) || Attributes.Num() < 1)
	{
		WaitForAttributeChangedTask->RemoveFromRoot();
		return nullptr;
	}

	for (FGameplayAttribute Attribute : Attributes)
	{
		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(Attribute)
			.AddUObject(WaitForAttributeChangedTask, &UAttributeChangedAsyncTask::AttributeChanged);
	}

	return WaitForAttributeChangedTask;
}

void UAttributeChangedAsyncTask::EndTask()
{
	if (IsValid(ASC))
	{
		ASC->GetGameplayAttributeValueChangeDelegate(AttributeToListenFor).RemoveAll(this);

		for (FGameplayAttribute Attribute : AttributesToListenFor)
		{
			ASC->GetGameplayAttributeValueChangeDelegate(Attribute).RemoveAll(this);
		}
	}

	SetReadyToDestroy();
	MarkPendingKill();
}

void UAttributeChangedAsyncTask::AttributeChanged(const FOnAttributeChangeData & Data)
{
	// Some times values are equals, do not inform
	if (FMath::IsNearlyEqual(Data.NewValue, Data.OldValue, 0.01f)) {
		return;
	}

	// If max attribute value is informed...
	if (MaxAttributeToCompare.IsValid())
	{
		// Check if NewValue is greater than max value
		float MaxValue = ASC->GetNumericAttribute(MaxAttributeToCompare);
		
		float NewValueClamped = FMath::Clamp(Data.NewValue, 0.0f, MaxValue);
		float OldValueClamped  = FMath::Clamp(Data.OldValue, 0.0f, MaxValue);

		OnAttributeChanged.Broadcast(Data.Attribute, NewValueClamped, OldValueClamped, (NewValueClamped - OldValueClamped));
	}
	else 
	{
		OnAttributeChanged.Broadcast(Data.Attribute, Data.NewValue, Data.OldValue, (Data.NewValue - Data.OldValue));
	}
}